DROP TABLE IF EXISTS InfoStudenti; 
DROP TABLE IF EXISTS InfoCazari;
DROP TABLE IF EXISTS InfoCamere;
DROP TABLE IF EXISTS Studenti;
DROP TABLE IF EXISTS Facultate;
DROP TABLE IF EXISTS Camere;
DROP TABLE IF EXISTS Camine;
DROP TABLE IF EXISTS Campusuri; 


CREATE TABLE Campusuri (
	id_Campus smallint NOT NULL 
		CONSTRAINT pk_id_Campus PRIMARY KEY,
	NumeCampus VARCHAR(24) NOT NULL,
	Cantina BOOLEAN,
	NrCamere NUMERIC(4)
	);

CREATE TABLE Camine (
	id_Camin smallint NOT NULL 
		CONSTRAINT pk_id_Camin PRIMARY KEY,
	NumeCamin VARCHAR(24) NOT NULL,
	Adresa VARCHAR(128),
	NrEtaje smallint,
	NrCamere NUMERIC (4),
	NumeAdm VARCHAR (64),
	id_Campus smallint
		CONSTRAINT fk_camine REFERENCES Campusuri(id_Campus)
	);

CREATE TABLE Camere (
	id_Camera NUMERIC (4) NOT NULL
		CONSTRAINT pk_id_Camere PRIMARY KEY,
	NrPersCam smallint,
	NrLocOcup smallint,
	NrLocLibere smallint,
	Baie BOOLEAN,
	TipCamera CHAR(1) DEFAULT 'M' NOT NULL CONSTRAINT ck_tipcamera CHECK (TipCamera IN ('F', 'M')),
	id_Camin smallint
		CONSTRAINT fk_Camere REFERENCES Camine(id_Camin)
	);
	
CREATE TABLE Facultate (
	id_Facultate smallint NOT NULL
		CONSTRAINT pk_id_Facultate PRIMARY KEY,
	NumeFac VARCHAR(48) NOT NULL,
	NumeDecan VARCHAR(64)
	);

CREATE TABLE Studenti (
	id_Stud smallint NOT NULL
		CONSTRAINT pk_id_Studenti PRIMARY KEY,
	NumeStud VARCHAR(24) NOT NULL,
	PrenumeStud VARCHAR(24) NOT NULL,
	CNP NUMERIC (13) NOT NULL,
	Sex CHAR(1) DEFAULT 'M' NOT NULL CONSTRAINT ck_sex CHECK (Sex IN ('F', 'M')),
	Adresa VARCHAR(128),
	id_Camera smallint
		CONSTRAINT fk_Studenti REFERENCES Camere(id_Camera),
	id_Facultate smallint
		CONSTRAINT fk_Studenti1 REFERENCES Facultate(id_Facultate)
	);


CREATE TABLE InfoCamere(
	id_Camin smallint NOT NULL,
		 /*CONSTRAINT pk_id_Camin PRIMARY KEY*/
	id_Camera smallint NOT NULL,
		/*CONSTRAINT pk_id_Camera PRIMARY KEY,*/
	Etaj smallint NOT NULL,
		/*CONSTRAINT pk_Etaj PRIMARY KEY,*/
	NrCamera NUMERIC (4), 
	PRIMARY KEY (id_Camin, id_Camera, Etaj)
	);
	
CREATE TABLE InfoCazari (
	id_Camin smallint NOT NULL,
		/*CONSTRAINT pk_id_Camin PRIMARY KEY,*/
	id_Stud smallint NOT NULL,
		/*CONSTRAINT pk_id_Stud PRIMARY KEY,*/
	id_Camera NUMERIC (4) NOT NULL, 
		/*CONSTRAINT pk_id_Camera PRIMARY KEY,*/
	Chirie NUMERIC (4),
	DataCazare DATE NOT NULL,
		/*CONSTRAINT pk_DataCazare PRIMARY KEY,*/
	DataDecazare DATE,
	PRIMARY KEY (id_Camin, id_Stud, id_Camera, DataCazare)
	);
	
CREATE TABLE InfoStudenti (
	id_Camin smallint,
	id_Stud smallint NOT NULL,
		/*CONSTRAINT pk_id_Stud PRIMARY KEY,*/
	id_Facultate smallint NOT NULL,
		/*CONSTRAINT pk_id_Facultate PRIMARY KEY,*/
	Medie NUMERIC(4),
	PRIMARY KEY (id_Stud, id_Facultate)
	);


/* Campusuri */
INSERT INTO Campusuri (id_Campus, NumeCampus, Cantina, NrCamere) 
	VALUES (1, 'Targusor-Copou', TRUE, 1750);
INSERT INTO Campusuri (id_Campus, NumeCampus, Cantina, NrCamere) 
	VALUES (2, 'Titu Maiorescu', TRUE, 2286);
INSERT INTO Campusuri (id_Campus, NumeCampus, Cantina, NrCamere) 
	VALUES (3, 'Codrescu', FALSE, 1800);
INSERT INTO Campusuri (id_Campus, NumeCampus, Cantina, NrCamere) 
	VALUES (4, 'Gaudeamus', TRUE, 400);
INSERT INTO Campusuri (id_Campus, NumeCampus, Cantina, NrCamere) 
	VALUES (5, 'Akademos', TRUE, 500);

/* Camine */
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (1, 'C1', 'Str. Stoicescu nr.1-4' , 3 , 412, 'Georgeta Ailenii', 1);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (2, 'C2', 'Str. Stoicescu nr.1-4' , 3, 449, 'Georgeta Ailenii', 1);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (3, 'C3', 'Str. Stoicescu nr.1-4' , 3, 417, 'Elena Ciobanu', 1);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (4, 'C4', 'Str. Stoicescu nr.1-4' , 3, 436, 'Elena Ciobanu', 1);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (5, 'C5', 'Str. Titu Maiorescu nr.7-9' , 4, 1356, 'Maria-Mihaela Tuiu', 2);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (6, 'C6', 'Str. Titu Maiorescu nr.7-9' , 4, 1356, 'Maria-Mihaela Tuiu', 2);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (7, 'C7', 'Str. Titu Maiorescu nr.7-9' , 4, 1357, 'Ec. Doina-Elena Epure', 2);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (8, 'C8', 'Str. Titu Maiorescu nr.7-9' , 4, 1357, 'Ec. Doina-Elena Epure', 2);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (9, 'C10', 'Str.Codrescu, nr. 7 ' , 5, 453, 'Ec. Valeria Ciobanu', 3);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (10, 'C11', 'Str.Codrescu, nr. 7 ' , 5, 316, 'Ec. Mihaela-Lenuta Boureanu', 3);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (11, 'C12', 'Str.Codrescu, nr. 7 ' , 5, 557, 'Ec. Mihaela-Lenuta Boureanu', 3);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (12, 'C13', 'Str.Codrescu, nr. 7 ' , 5, 463, 'Dana-Mihaela Raba', 3);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (13, 'Gaudeamus', 'Str. Codrescu Nr.1' , 2, 400, 'Ec. Teodora Tanasa', 4);
INSERT INTO Camine (id_Camin, NumeCamin, Adresa, NrEtaje, NrCamere, NumeAdm, id_Campus) 
	VALUES (14, 'Akademos', 'Str. Pacurari, nr. 6' , 2, 500, 'Ec. Teodora Tanasa', 5);

/* Camere */
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (1, 4, 4, 0, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (2, 4, 3, 1, FALSE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (3, 4, 2, 2, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (4, 4, 1, 3, TRUE, 'M', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (5, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (6, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (7, 3, 2, 1, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (8, 3, 3, 0, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (9, 3, 2, 1, TRUE, 'F', 1);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (100, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (101, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (102, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (103, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (104, 4, 4, 0, FALSE, 'M', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (105, 5, 4, 1, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (106, 5, 4, 1, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (107, 4, 4, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (108, 4, 4, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (109, 5, 5, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (200, 5, 5, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (201, 4, 3, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (202, 4, 4, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (203, 4, 2, 2, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (204, 2, 2, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (205, 2, 2, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (206, 3, 3, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (207, 3, 3, 0, FALSE, 'F', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (208, 3, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (209, 3, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (300, 4, 4, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (301, 3, 3, 0, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (302, 5, 4, 1, FALSE, 'F', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (303, 2, 2, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (304, 4, 2, 1, FALSE, 'M', 3);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (305, 5, 5, 0, FALSE, 'F', 2);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (306, 3, 2, 1, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (307, 4, 2, 2, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (308, 4, 4, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (309, 4, 4, 0, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (400, 5, 5, 0, FALSE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (401, 4, 3, 1, TRUE, 'M', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (402, 3, 3, 0, TRUE, 'F', 4);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (403, 4, 3, 1, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (404, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (405, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (406, 4, 3, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (407, 5, 4, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (408, 4, 4, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (409, 5, 5, 0, FALSE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (500, 5, 5, 0, FALSE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (501, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (502, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (503, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (504, 3, 3, 0, TRUE, 'F', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (505, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (506, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (507, 2, 2, 0, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (508, 3, 2, 1, TRUE, 'M', 5);
INSERT INTO Camere (id_Camera, NrPersCam, NrLocOcup, NrLocLibere, Baie, TipCamera, id_Camin)  VALUES (509, 3, 3, 0, TRUE, 'M', 5);

/* Facultati */
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (1, 'Biologie', 'Glotariu Cristi' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (2, 'Chimie', 'Bladul Cornel' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (3, 'Drept', 'Gicu Gheorghe' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (4, 'Economie si Administrarea Afacerilor', 'Dinu Airiniei' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (5, 'Educatie Fizica si Sport', 'Klaus Koro' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (6, 'Filosofie si Stiinte Social-Politice', 'Buhai Groapa' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (7, 'Fizica', 'Grigorescu Clerul' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (8, 'Geografie si Geologie', 'Pasarescu Cristi' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (9, 'Informatica', 'Bidon Ionut' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (10, 'Istorie', 'Teava Andrei' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (11, 'Litere', 'Servetel Toma' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (12, 'Matematica', 'Mihail Mihailescu' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (13, 'Psihologie si Stiint ale Educatiei', 'Drob Petru' );
INSERT INTO Facultate (id_Facultate, NumeFac, NumeDecan)
	VALUES (14, 'Teologie', 'Trompeta Grigoras' );


/* Studenti */
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (1, 'Stroe', 'Mihaela', 2601228390834, 'F', 'Bd. Cantemir, 32, Bl.G4, Sc.C, Ap.4', 205, 3) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (2, 'Buzatu', 'Corneliu', 1650512370514, 'Mn', 'Str. Desprimaveririi, 112', 109, 1) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (3, 'Spineanu', 'Marius', 5010101380625, 'M', 'Bd. Stefan cel Mare, 4, Bl.I1, Sc.A, Ap.24', 8, 6) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (4, 'Bagdasar', 'Adela', 2601002250611, 'F', 'Str. Primaverii, 17', 304, 6) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (5, 'Ionascu', 'Ionelia', 6001122390199, 'F', 'Str. Florilor', 203, 8) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (6, 'Popesc', 'Maria-Mirabela', 2721231300888, 'F', 'Bd. 22 Decembrie, 2, Bl.5, Sc.B, Ap.21', 509, 9) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (7, 'Stroescu', 'Mihaela Oana', 6020719120545, 'F', 'Str. Lenei Nr. 234', 305, 5) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (8, 'Nicolae', 'Ana Maria', 2690202200345, 'F', 'Bd. Stroe, 49, Bl.Y3, Sc.A, Ap.97', 402, 7) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (9, 'Ciorba', 'Cristina', 2690202200345, 'F', 'Str. Clas, 72, Bl.D3, Sc.1, Ap.3', 403, 10) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (10, 'Claudia', 'Mihaela', 2690202200345, 'F', 'Bd. Flak, 13, Bl.K4, Sc.1, Ap.16', 4, 12) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (11, 'Torta', 'Andreea', 2690202200345, 'F', 'Str. Plos, 46, Bl.PO7, Sc.7, Ap.34', 301, 8) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (12, 'Calmasan', 'Stefan', 5010101380625, 'M', 'Bd. Stefan cel Mare, 1, Bl.DF1, Sc.4, Ap.53', 104, 5) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (13, 'Droaba', 'Claudiu', 5010101380625, 'M', 'Bd. Banu, 4, Bl.B4, Sc.1, Ap.1', 108, 6) ;
INSERT INTO Studenti (id_Stud, NumeStud, PrenumeStud, CNP, Sex, Adresa, id_Camera, id_Facultate)
	VALUES (14, 'Gloaba', 'Gicu', 5010101380625, 'M', 'Bd. Groza, 4, Bl.B8, Sc.3, Ap.23', 305, 7) ;

/* InfoCamere */
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 1, 0, 1);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 2, 0, 2);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 3, 0, 3);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 4, 0, 4);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 5, 0, 5);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 6, 0, 6);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 7, 0, 7);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 8, 0, 8);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (1, 9, 0, 9);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 100, 1, 100);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 101, 1, 101);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 102, 1, 102);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 103, 1, 103);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 104, 1, 104);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 105, 1, 105);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 106, 1, 106);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 107, 1, 107);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 108, 1, 108);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (2, 305, 1, 305);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 109, 1, 109);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 200, 1, 200);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 201, 1, 201);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 202, 1, 202);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 206, 1, 206);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 207, 1, 207);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 208, 1, 208);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 209, 1, 209);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 300, 1, 300);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 301, 1, 301);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 302, 1, 302);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (3, 304, 1, 304);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 306, 1, 306);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 307, 1, 307);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 308, 1, 308);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 309, 1, 309);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 400, 1, 400);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 401, 1, 401);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (4, 402, 1, 402);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 403, 1, 403);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 404, 1, 404);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 405, 1, 405);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 406, 1, 406);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 407, 1, 407);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 408, 1, 408);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 409, 1, 409);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 500, 1, 500);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 501, 1, 501);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 502, 1, 502);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 503, 1, 503);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 504, 1, 504);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 505, 1, 505);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 506, 1, 506);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 507, 1, 507);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 508, 1, 508);
INSERT INTO InfoCamere (id_Camin, id_Camera, Etaj, NrCamera) VALUES (5, 509, 1, 509);

/* InfoCazari */
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 1, 1, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 2, 2, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 3, 3, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 4, 4, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 5, 5, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 6, 6, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (1, 7, 7, 199, '2017-02-12', '2018-04-30');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (3, 8, 300, 299, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (3, 9, 300, 299, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (4, 10, 402, 399, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (4, 11, 402, 399, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 12, 404, 499, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 13, 404, 499, '2017-05-19', '2018-07-31');
INSERT INTO InfoCazari (id_Camin, id_Stud, id_Camera, Chirie, DataCazare, DataDecazare)
	VALUES (5, 14, 500, 499, '2017-01-01', '2018-09-30');

/* InfoStudenti */
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 1, 3, 7.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 2, 1, 6.46);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 3, 6, 7.72);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 4, 6, 8.34);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 5, 8, 6.86);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 6, 9, 5.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (1, 7, 5, 8.53);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (3, 8, 7, 7.76);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (3, 9, 10, 8.53);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (4, 10, 12, 8.87);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (4, 11, 8, 8.99);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 12, 5, 9.54);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 13, 6, 9.87);
INSERT INTO InfoStudenti (id_Camin, id_Stud, id_Facultate, Medie) VALUES (5, 14, 7, 9.99);